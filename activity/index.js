students = []

function addStudent(name){
	students.unshift(name);
	console.log(`${name} was added to the student's list.`);
};

function countStudents(){
	studentCount = 0
	students.forEach(function(student){
		studentCount++
	});
	console.log(`There are a total of ${studentCount} students enrolled.`);
};

function printStudents(){
	students.sort().forEach(function(student){
		console.log(student)
	});
};

function findStudent(keyword){

	let searchResults = students.filter(function(student){
		return (student.toLowerCase().indexOf(keyword.toLowerCase()) > -1);
	});

	if (searchResults.length === 1){
		console.log(`${searchResults.toString()} is an Enrollee.`);
	} else if (searchResults.length > 1){
		console.log(`${searchResults.join(", ")} are Enrollees.`);
	} else {
		console.log(`No student found with the name ${keyword}.`);
	}

};